# zonapi.alumnos.udl

Api of the teacher evaluation system, part of the student evaluation service

**Change the default data for production on the server**
_config/env/config.php_

```php

$config_api => array(
    'hostname'=>'domain.com',
    'key'=>'used your key',
    'error_debug'=>false
);

```
